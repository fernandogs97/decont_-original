# Creación de directorios 
mkdir -p out/merged
mkdir -p log/cutadapt
mkdir -p out/trimmed

# Descarga de todos los archivos dentro de data/urls
wget -c -i data/urls -P data

# Descarga del archivo de contaminantes en formato fasta + descompresión 
if [ -f res/contaminants.fasta ]
then
echo "#Nota: Ya se descargó la secuencia de contaminantes en procesamientos previos"
else
bash scripts/download.sh https://bioinformatics.cnio.es/data/courses/decont/contaminants.fasta.gz res yes 
fi

# Realización del índice a partir de contaminants_idx 
if [ -d res/contaminants_idx ];
then
echo "#Nota: Ya se realizó contaminants_idx en procesamientos previos"
else
bash scripts/index.sh res/contaminants.fasta res/contaminants_idx
fi

# Unión de las muestras en un único archivo 
for sid in $(ls data/*.fastq.gz | cut -d "-" -f1 | cut -d "/" -f2 | sort | uniq) 
do
    if [ -f out/merged/${sid}.fastq.gz ];
    then
    echo "#Nota: Unión realizada previamente para la muestra ${sid}"
    else 
    bash scripts/merge_fastqs.sh data out/merged $sid
    fi
done

# Cutadapt para las muestras unidas 
for merged in $(ls out/merged/*.fastq.gz | cut -d "." -f1 | cut -d "/" -f3 | sort | uniq)
do 
    if [ -f log/cutadapt/${merged}.log ];
    then
    echo "#Nota: Cutadapt realizado previamente para la muestra ${merged}"
    else
    echo "Realizando cutadapt para muestra ${merged}" 
    cutadapt -m 18 -a TGGAATTCTCGGGTGCCAAGG --discard-untrimmed -o out/trimmed/${merged}.trimmed.fastq.gz out/merged/${merged}.fastq.gz > log/cutadapt/${merged}.log
    fi
done

# STAR para todas las muestras trimmed 
for fname in out/trimmed/*.fastq.gz
do
    sid=$(basename $fname | cut -d "." -f1)
    if [ -d out/star/${sid} ];
    then
    echo "#Nota: STAR realizado para la muestra ${sid}"
    else
    echo "Realizando STAR para muestra ${sid}"
    mkdir -p out/star/$sid
    STAR --runThreadN 6 --genomeDir res/contaminants_idx --outReadsUnmapped Fastx --readFilesIn out/trimmed/${sid}.trimmed.fastq.gz --readFilesCommand gunzip -c --outFileNamePrefix out/star/${sid}/
    fi 
done 

# Guardado de los resultados de cutadapt y star en fichero log.out
for url in $(sort data/urls | cut -d "/" -f7 | cut -d "-" -f1 | uniq)
do      
    if [ -f log.out ];
    then 
    echo "Actualizando archivo log.out" 
    id=$(grep $url log.out | cut -d " " -f2 | cut -d "#" -f1)
    if [ "$id" == "$url" ];
    then
    echo "#Nota: La muestra ${url} ya se ha incluido en log.out previamente"  
    else 
    echo "####################Muestra ${url}#####################" >> log.out; echo "" >> log.out
    echo "#Cutadapt logs" >> log.out; echo "" >> log.out 
    grep "^Reads with adapters" log/cutadapt/${url}.log >> log.out; echo "" >> log.out
    grep "^Total basepairs processed" log/cutadapt/${url}.log >> log.out
    echo "#Star logs" >> log.out; echo "" >> log.out
    grep "Uniquely mapped reads %" out/star/${url}/Log.final.out >> log.out; echo "" >> log.out
    grep "% of reads mapped to multiple loci" out/star/${url}/Log.final.out >> log.out; echo "" >> log.out
    grep "% of reads mapped to too many loci" out/star/${url}/Log.final.out >> log.out; echo "" >> log.out; echo "" >> log.out
    fi
    else
    echo "Creando archivo log.out"
    echo "LOGS DE LAS MUESTRAS ANALIZADAS" > log.out; echo >> log.out
    echo "####################Muestra ${url}#####################" >> log.out; echo "" >> log.out
    echo "#Cutadapt logs" >> log.out; echo "" >> log.out 
    grep "^Reads with adapters" log/cutadapt/${url}.log >> log.out; echo "" >> log.out
    grep "^Total basepairs processed" log/cutadapt/${url}.log >> log.out
    echo "#Star logs" >> log.out; echo "" >> log.out
    grep "Uniquely mapped reads %" out/star/${url}/Log.final.out >> log.out; echo "" >> log.out
    grep "% of reads mapped to multiple loci" out/star/${url}/Log.final.out >> log.out; echo "" >> log.out
    grep "% of reads mapped to too many loci" out/star/${url}/Log.final.out >> log.out; echo "" >> log.out; echo "" >> log.out
    fi
done
