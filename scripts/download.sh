if [ "$3" == "yes" ]; then
    wget -O - $1 | gunzip -N -c > $2/contaminants.fasta
else
    wget $1 -P $2
fi
